### Vacancies Clustering and Analysis

Goal of the project is to extract job profiles and trends from the data set of about 5 million job vacancies from the period of
two years. The basic idea is to find similar jobs, group them, find representative skills in each group, and possibly
tell whether popularity of it is rising or falling.

Formatted output of clustering of all 5M vacancies should be available at [marquis.ijs.si:8434](http://marquis.ijs.si:8434/).

Few explanations:

* When run for the first time, hierarchical K-means is run and clusters are dumped into `clusters` folder. In next runs clusters are 
only loaded from there.
* In every run clusters are analyzed, output is saved to `out/output.json`.
* `external` folder contains code for parsing json.
* `vacancies/skills_sample.json` is an array of names of skills.
* `vacancies/vacancies_sample` contains sample of 5000 vacancies in the following format (all values are seperated by space):
```
no_of_jobs
(for each job):
    day
    month
    year
    no_of_skills
    (for each skill):
        index_in_skills.json
        pageRank
```
* `out/output.json` is the output of the program. It has the following format:
```
{
  'no_months': int,
  'no_vacancies': int,
  'skills': [
    {
      'index': int,
      'k': double,
      'n': double,
      'name': string,
      'no_occurrences': int,
      'no_different_clusters': int,
      'avg_rank': double,
      'graph': [],
      'popularity': []
    },
    ...
  ],
  'clusters': [
    {
      'id': int,
      'k': double,
      'n': double,
      'greatness': double,
      'depth': int,
      'children': [int],
      'no_vacancies': int,
      'skills': [
        {
          'index': int,
          'TF': int,
          'TFIDF': double
        },
        ...
      ],
      'graph': []
    },
    ...
  ]
}
```

