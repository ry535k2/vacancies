#include "K-means.h"

namespace k_means {

Cluster::Cluster(Cluster *parent) {
  coordinates.assign(constants.no_skills, 0);
  depth = parent ? parent->depth + 1 : 0;
}

double Cluster::getSimilarity(Vacancy *vacancy) {
  double ret = 0;
  for (int i : vacancy->skills)
    ret += coordinates[i];
  if (!vacancy->pageRanks.empty())
    ret /= vacancy->pageRanks.size();
  return ret;
}

void Cluster::moveToCenter() {
  if (new_vacancies.empty())
    return;

  std::fill(coordinates.begin(), coordinates.end(), 0);

  vacancies.splice(vacancies.begin(), new_vacancies);

  for (Vacancy *vacancy : vacancies)
    for (int i : vacancy->skills)
      ++coordinates[i];

  double sum = 0;
  for (double s : coordinates)
    sum += s;
  if (sum > 0)
    for (int i = 0; i < constants.no_skills; ++i)
      coordinates[i] /= sum;
}

K_means::K_means() {
  root = new Cluster();
  root->vacancies.assign(Reading::vacancies.begin(), Reading::vacancies.end());
  clusters.push_back(root);
  KMeans(root);
}

bool converge(std::list<Cluster *> &children) {
  int iteration_counter = 0, no_moved;

  do {
    if (report())
      print(str(iteration_counter) + ". iteration");

    // moving
    for (Cluster *from : children) {
      for (auto iter = from->vacancies.begin(); iter != from->vacancies.end();) {
        Vacancy *vacancy = *iter;

        Cluster *better = nullptr;
        double current = from->getSimilarity(vacancy);

        for (Cluster *to : children) {
          if (from == to) continue;

          double similarity = to->getSimilarity(vacancy);
          if (similarity > current) {
            better = to;
            current = similarity;
          }
        }

        if (better) {
          better->new_vacancies.push_back(vacancy);
          iter = from->vacancies.erase(iter);
        } else ++iter;
      }
    }

    no_moved = 0;
    for (Cluster *child : children)
      no_moved += child->new_vacancies.size();
    if (report())
      print("no_moved: " + str(no_moved));

    // moving to center
    for (Cluster *child : children)
      child->moveToCenter();

    // deleting small
    for (auto iter = children.begin(); iter != children.end();) {
      Cluster *child = *iter;

      if (child->vacancies.size() < Constants::min_vacancies_in_cluster) {

        // assigning vacancies to siblings
        for (Vacancy *vacancy : child->vacancies) {

          Cluster *best = nullptr;
          double current = 0;

          for (Cluster *to : children) {
            if (child == to) continue;

            double similarity = to->getSimilarity(vacancy);
            if (!best || similarity > current) {
              best = to;
              current = similarity;
            }
          }

          if (best)
            best->vacancies.push_back(vacancy);
        }

        iter = children.erase(iter);
        delete child;
      } else ++iter;
    }

    if (children.size() < 2)
      return false;

  } while (no_moved > Constants::min_moved_vacancies &&
      ++iteration_counter < Constants::max_converge_iterations);

  if (iteration_counter >= Constants::max_converge_iterations) {
    print("max iterations, probably cycle");
    return false;
  }

  return true;
}

void K_means::KMeans(Cluster *cluster) {

  if (report())
    print(str(clusters.size()) + " clusters.");

  for (int i = 0; i < Constants::dividing_attempts; ++i) {

    // creating children
    for (int j = 0; j < Constants::no_children; ++j)
      cluster->children.push_back(new Cluster(cluster));

    // assigning vacancies
    auto iter = cluster->children.begin();
    for (Vacancy *vacancy : cluster->vacancies) {
      (*iter)->new_vacancies.push_back(vacancy);

      ++iter;
      if (iter == cluster->children.end())
        iter = cluster->children.begin();
    }

    for (Cluster *child : cluster->children)
      child->moveToCenter();

    if (!converge(cluster->children)) {
      for (Cluster *child : cluster->children)
        delete child;
      cluster->children.clear();
      continue;
    }

    for (Cluster *child : cluster->children)
      clusters.push_back(child);

    for (Cluster *child : cluster->children)
      if (child->vacancies.size() > 2 * Constants::min_vacancies_in_cluster)
        KMeans(child);

    return;
  }
}

}
