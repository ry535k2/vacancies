#ifndef K_MEANS_H
#define K_MEANS_H

#include "../reading/Reading.h"

namespace k_means {

class Cluster {
 public:
  std::vector<double> coordinates;
  std::list<Vacancy *> vacancies, new_vacancies;
  std::list<Cluster *> children;
  int index = -1, depth;

  explicit Cluster(Cluster * = nullptr);

  double getSimilarity(Vacancy *);

  void moveToCenter();
};

class K_means {
 public:
  std::list<Cluster *> clusters;
  Cluster *root;

  K_means();

  void KMeans(Cluster *);
};

}

#endif  // K_MEANS_H
