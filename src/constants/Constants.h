#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <iostream>
#include <string>
#include <cstring>
#include <set>
#include <map>
#include <utility>
#include <vector>
#include <algorithm>
#include <queue>
#include <forward_list>
#include <list>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <utility>
#include <sstream>
#include <tuple>
#include <iomanip>
#include <numeric>
#include <random>
#include <stdexcept>

#include "../../external/json_parser/json.h"

#define WRITE
#define COUT
#define DELETE_OLD
#define SAMPLE

#define str std::to_string
#define ri(x) str((int) std::round(x))

using std::string;
using std::abs;

class Constants {
 public:
#ifdef SAMPLE
  static const bool force_clustering = true;
#else
  static const bool force_clustering = false;
#endif

  // paths
#ifdef SAMPLE
  const string vacancies_path = "../vacancies/vacancies_sample";
  const string skills_path = "../vacancies/skills_sample.json";
  const string output_path = "../out/output_sample.json";
#else
  const string vacancies_path = "../vacancies/vacancies";
  const string skills_path = "../vacancies/skills.json";
  const string output_path = "../out/output.json";
#endif
  const string log_file = "../out/stdout";
  string clusters_path;

  // reading settings
  int no_skills;
  static const int max_skills = 3000;
  static const int max_vacancies = 0;  // 0 for no limit
  static const int min_skills_in_vacancy = 2;
  static const int months_shift = 199;
  static const int months_range = 20;

  // K-means settings
  static const int no_children = 10;
  static const int dividing_attempts = 3;
  static const int min_moved_vacancies = 20;
  static const int max_converge_iterations = 1000;
  static const int min_vacancies_in_cluster = 500;

  // cluster settings
  static const int max_skills_in_cluster = 55;
  static const int min_skills_in_cluster = 35;
  static const int min_skill_count_in_cluster = 30;
  static const int skills_in_greatness = 7;

  Constants();
};

class Time {
 public:
  string text;
  time_t start_time;

  explicit Time(string text);

  void out();
};

class LinearRegression {
  double x = 0, xx = 0, y = 0, yy = 0, xy = 0, count = 0;

 public:
  void addPoint(double, double);

  double getK();

  double getN();

  double getD();

  void reset();
};

extern Constants constants;

bool report();

void print(const string & = "", const string & = "\n");

void print(const int &, const string & = "\n");

void print(const double &, const bool & = true, const string & = "\n");

#endif  // CONSTANTS_H
