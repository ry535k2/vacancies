#include "Constants.h"

Constants constants;

Constants::Constants() {
#ifdef DELETE_OLD
  remove(constants.log_file.c_str());
#endif

  if (max_vacancies)
    clusters_path = "../clusters/clusters_with_" + str(max_vacancies) + "_jobs";
  else
    clusters_path = "../clusters/clusters";

#ifdef SAMPLE
  clusters_path += "_sample";
#endif

  constants.no_skills = Constants::max_skills;
}

Time::Time(string text) : text(text) {
  print("Start of " + text + ".");
  start_time = time(nullptr);
}

void Time::out() {
  print("End of " + text +
      ". (" + str(time(nullptr) - start_time) + " s)");
}

void LinearRegression::addPoint(double i, double j) {
  x += i;
  xx += i * i;
  y += j;
  yy += j * j;
  xy += i * j;
  count++;
}

double det(double mat[2][2]) {
  return mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];
}

double LinearRegression::getK() {
  double A[2][2] = {{xx, x},
                    {x, count}};
  double A1[2][2] = {{xy, x},
                     {y, count}};

  return det(A1) / det(A);
}

double LinearRegression::getN() {
  double A[2][2] = {{xx, x},
                    {x, count}};
  double A2[2][2] = {{xx, xy},
                     {x, y}};

  return det(A2) / det(A);
}

double LinearRegression::getD() {
  double A[2][2] = {{xx, x},
                    {x, count}};
  double A1[2][2] = {{xy, x},
                     {y, count}};
  double A2[2][2] = {{xx, xy},
                     {x, y}};

  double k = getK();
  double n = getN();

  return yy + k * k * xx + count * n * n - 2 * k * xy - 2 * n * y + 2 * k * n * x;
}

void LinearRegression::reset() {
  x = 0;
  xx = 0;
  y = 0;
  yy = 0;
  xy = 0;
  count = 0;
}

bool report() {
  static time_t last_report = time(nullptr);
  if (time(nullptr) - last_report > 2) {
    last_report = time(nullptr);
    return true;
  }
  return false;
}

void print(const string &text, const string &end) {
#ifdef COUT
  std::cout << text << end;
#endif

#ifdef WRITE
  std::ofstream file(constants.log_file, std::ofstream::app);
  file << text << end;
  file.close();
#endif
}

void print(const int &number, const string &end) {
  print(str(number), end);
}

void print(const double &number, const bool &round, const string &end) {
  print(round ? ri(number) : str(number), end);
}
