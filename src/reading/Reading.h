#ifndef MAIN_H
#define MAIN_H

#include "../constants/Constants.h"

class Vacancy {
 public:
  int index = -1, timestamp = 0;  // (months from 2000) - `from`
  std::forward_list<int> skills;
  std::map<int, double> pageRanks;

  void setTimestamp(int, int, int);

  void assignSkills();
};

class Skill {
 public:
  string name;
  int i, no_occurrences = 0, no_different_clusters = 0;
  double k = 0, n = 0, avg_graph = 0, avg_rank = 0, IDF = 0;
  std::vector<double> graph, popularity;

  Skill(string name, int i) : name(name), i(i) {}
};

class Reading {
 public:
  static std::list<Vacancy *> vacancies;
  static std::vector<Skill *> skills;

  static void read();
};

#endif  // MAIN_H
