#include "Reading.h"

std::list<Vacancy *> Reading::vacancies;
std::vector<Skill *> Reading::skills;

void Vacancy::setTimestamp(int, int month, int year) {
  timestamp = (year - 2000) * 12 + (month - 1) - Constants::months_shift;
}

void Vacancy::assignSkills() {
  for (const auto &pair : pageRanks)
    skills.push_front(pair.first);
}

void Reading::read() {

  // names of skills
  std::ifstream skills_file(constants.skills_path);
  Json::Value skills_json;
  skills_file >> skills_json;
  std::vector<string> skill_names;
  for (auto &skill : skills_json)
    skill_names.push_back(skill.asString());

  // counts
  std::vector<int> counts(skills_json.size(), 0);

  // reading vacancies
  std::ifstream vacancies_stream(constants.vacancies_path);
  int no_vacancies;
  vacancies_stream >> no_vacancies;

  for (int i = 0; i < no_vacancies; ++i) {
    int day, month, year, no_skills;

    // feedback
    if (report())
      print(ri((double) i / no_vacancies * 100) + " %");

    // title
    auto *vacancy = new Vacancy();

    // date
    vacancies_stream >> day >> month >> year;
    vacancy->setTimestamp(day, month, year);

    bool add = vacancy->timestamp >= 0 && vacancy->timestamp < Constants::months_range;

    // skills
    vacancies_stream >> no_skills;
    for (int j = 0; j < no_skills; ++j) {
      int skill;
      double pageRank;

      vacancies_stream >> skill >> pageRank;
      if (add) {
        vacancy->pageRanks[skill] = pageRank;
        ++counts[skill];
      }
    }

    // adding vacancy
    if (add && !vacancy->pageRanks.empty()) {
      vacancies.push_back(vacancy);
      if (Constants::max_vacancies && vacancies.size() >= Constants::max_vacancies) break;
    } else
      delete vacancy;
  }

  // finding top n skills
  std::list<int> indexes(skill_names.size(), 0);
  int i = 0;
  for (int &index : indexes)
    index = i++;
  indexes.sort([&counts](const int a, const int b) {
    return counts[a] > counts[b];
  });
  std::set<int> top_n_indexes;
  int no_skills = constants.no_skills;
  for (int index : indexes) {
    if (!no_skills--)
      break;
    top_n_indexes.insert(index);
  }

  // deleting skills with low count
  for (auto vacancies_iter = vacancies.begin(); vacancies_iter != vacancies.end();) {
    Vacancy *vacancy = *vacancies_iter;

    for (auto ranks_iter = vacancy->pageRanks.begin(); ranks_iter != vacancy->pageRanks.end();)
      if (top_n_indexes.find(ranks_iter->first) == top_n_indexes.end())
        ranks_iter = vacancy->pageRanks.erase(ranks_iter);
      else ++ranks_iter;

    if (vacancy->pageRanks.size() < Constants::min_skills_in_vacancy) {
      delete vacancy;
      vacancies_iter = vacancies.erase(vacancies_iter);
    } else ++vacancies_iter;
  }

  // remapping skills
  std::vector<int> old_to_new(skills_json.size(), -1);
  for (Vacancy *vacancy : vacancies) {
    std::map<int, double> temp;
    temp.swap(vacancy->pageRanks);
    for (const auto &pair : temp) {
      int index = old_to_new[pair.first];
      if (index == -1) {
        index = (int) skills.size();
        old_to_new[pair.first] = index;
        skills.push_back(new Skill(skill_names[pair.first], index));
      }
      vacancy->pageRanks[index] = pair.second;
    }
  }
  constants.no_skills = (int) skills.size();

  // assigning skills
  i = 0;
  for (Vacancy *vacancy : vacancies) {
    vacancy->index = i++;
    vacancy->assignSkills();
  }

  print(str(vacancies.size()) + " vacancies read.");
}
