#include "Analyzing.h"

Cluster *Analyzing::root;
std::list<Cluster *> Analyzing::clusters;

Analyzing::Analyzing() {
  analyze();
}

void Analyzing::setClusters(Cluster *cluster) {
  clusters.push_back(cluster);
  for (Cluster *child : cluster->children)
    setClusters(child);
}

void Analyzing::clearClusters(Cluster *cluster) {
  clusters.push_back(cluster);
  for (auto iter = cluster->children.begin(); iter != cluster->children.end();) {
    Cluster *child = *iter;
    if (child->skills.size() >= Constants::min_skills_in_cluster) {
      clearClusters(child);
      ++iter;
    } else
      iter = cluster->children.erase(iter);
  }
}

void Analyzing::calculateTFIDF(bool calculateTF) {

  // TF
  if (calculateTF) {
    std::vector<int> TF(constants.no_skills, 0);
    for (Cluster *cluster : clusters) {
      std::fill(TF.begin(), TF.end(), 0);

      for (Vacancy *vacancy : cluster->vacancies)
        for (int i : vacancy->skills)
          ++TF[i];

      for (int i = 0; i < constants.no_skills; ++i) {
        if (TF[i] > Constants::min_skill_count_in_cluster) {
          auto *skill = new SkillInCluster(i);
          skill->TF = TF[i];
          cluster->skills.push_back(skill);
        }
      }
    }

    // clearing clusters
    clusters.clear();
    clearClusters(root);
  }

  // IDF
  for (int i = 0; i < constants.no_skills; ++i)
    Reading::skills[i]->no_different_clusters = 0;
  for (Cluster *cluster : clusters)
    for (SkillInCluster *skill : cluster->skills)
      ++skill->skill->no_different_clusters;

  for (Skill *skill : Reading::skills)
    if (skill->no_different_clusters) {
      skill->IDF = std::log((double) clusters.size() / skill->no_different_clusters);

      // test
      if (skill->IDF < 0)
        throw std::invalid_argument("IDF is negative.");
    }

  // TFIDF
  for (Cluster *cluster : clusters)
    for (SkillInCluster *skill : cluster->skills)
      skill->TFIDF = (double) skill->TF * skill->skill->IDF;
}

void Analyzing::analyze() {

  // assigning clusters
  clusters.clear();
  setClusters(root);

  // test
  for (Vacancy *vacancy : Reading::vacancies)
    if (vacancy->timestamp < 0 || vacancy->timestamp >= Constants::months_range)
      throw std::invalid_argument("Vacancies not in range.");

  // SKILLS
  calculateTFIDF();
  for (Cluster *cluster : clusters)
    cluster->selectSkills();
  calculateTFIDF(false);
  for (Cluster *cluster : clusters) {
    cluster->normalizeTFIDF();
    cluster->sortSkills();
  }

  // K IN SKILLS
  std::vector<int> all(Constants::months_range, 0);
  for (Vacancy *vacancy : Reading::vacancies)
    all[vacancy->timestamp] += vacancy->pageRanks.size();

  for (Skill *skill : Reading::skills)
    skill->graph.assign(Constants::months_range, 0);

  for (Vacancy *vacancy : Reading::vacancies)
    for (int i : vacancy->skills) {
      ++Reading::skills[i]->graph[vacancy->timestamp];
      ++Reading::skills[i]->no_occurrences;
    }

  for (int i = 0; i < constants.no_skills; ++i) {
    Skill *skill = Reading::skills[i];

    LinearRegression reg;
    double sum = 0;
    for (int j = 0; j < Constants::months_range; ++j) {
      skill->graph[j] = skill->graph[j] / all[j] / skill->no_occurrences;
      sum += skill->graph[j];
    }

    for (int j = 0; j < Constants::months_range; ++j) {
      skill->graph[j] /= sum;
      reg.addPoint(j, skill->graph[j]);
    }

    skill->k = reg.getK();
    skill->n = reg.getN();
  }

  // AVERAGE RANK, GREATNESS
  for (Vacancy *vacancy : Reading::vacancies)
    for (const auto &pair : vacancy->pageRanks)
      Reading::skills[pair.first]->avg_rank += pair.second;
  for (Skill *skill : Reading::skills)
    skill->avg_rank = std::log(skill->avg_rank / skill->no_occurrences);

  for (Cluster *cluster : clusters)
    cluster->calculateGreatness();

  // K IN CLUSTERS
  std::fill(all.begin(), all.end(), 0);
  for (Vacancy *vacancy : Reading::vacancies)
    ++all[vacancy->timestamp];

  for (Cluster *cluster : clusters) {
    cluster->graph.assign(Constants::months_range, 0);
    for (Vacancy *vacancy : cluster->vacancies)
      ++cluster->graph[vacancy->timestamp];

    double sum = 0;
    for (int i = 0; i < Constants::months_range; ++i) {
      cluster->graph[i] = cluster->graph[i] / all[i];
      sum += cluster->graph[i];
    }

    LinearRegression reg;
    for (int i = 0; i < Constants::months_range; ++i) {
      cluster->graph[i] /= sum;
      reg.addPoint(i, cluster->graph[i]);
    }

    cluster->k = reg.getK();
    cluster->n = reg.getN();
  }

  // POPULARITY OF SKILLS THROUGH TIME
  for (Skill *skill : Reading::skills) {
    skill->avg_graph = 0;
    for (int i = 0; i < Constants::months_range; ++i)
      skill->avg_graph += skill->graph[i];
    skill->avg_graph /= Constants::months_range;

    skill->popularity.assign(Constants::months_range, 0);
    for (int i = 0; i < Constants::months_range; ++i) {
      skill->popularity[i] = skill->graph[i] / skill->avg_graph;
      skill->popularity[i] *= skill->popularity[i];
    }
  }
}
