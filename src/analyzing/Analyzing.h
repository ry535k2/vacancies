#ifndef ANALYZING_H
#define ANALYZING_H

#include "../cluster/Cluster.h"

class Analyzing {
 public:
  static Cluster *root;
  static std::list<Cluster *> clusters;

  Analyzing();

  void setClusters(Cluster *);

  void clearClusters(Cluster *);

  void calculateTFIDF(bool calculateTF = true);

  void analyze();
};

#endif  // ANALYZING_H
