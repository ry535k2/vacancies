#ifndef EXPORT_H
#define EXPORT_H

#include "../analyzing/Analyzing.h"

class Export {
 public:
  std::list<Cluster *> clusters;

  Export();

  static void save_k_means(k_means::Cluster *);

  static void read_k_means();
};

#endif  // EXPORT_H
