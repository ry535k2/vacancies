#include "Export.h"


// EXPORTING CLUSTERS

/**
 * {
 *   'no_months': int,
 *   'no_vacancies': int,
 *   'skills': [
 *     {
 *       'index': int,
 *       'k': double,
 *       'n': double,
 *       'name': string,
 *       'no_occurrences': int,
 *       'no_different_clusters': int,
 *       'avg_rank': double,
 *       'graph': [],
 *       'popularity': []
 *     },
 *     ...
 *   ],
 *   'clusters': [
 *     {
 *       'id': int,
 *       'k': double,
 *       'n': double,
 *       'greatness': double,
 *       'depth': int,
 *       'children': [int],
 *       'no_vacancies': int,
 *       'skills': [
 *         {
 *           'index': int,
 *           'TF': int,
 *           'TFIDF': double
 *         },
 *         ...
 *       ],
 *       'graph': []
 *     },
 *     ...
 *   ]
 * }
 */
Export::Export() {
  clusters.splice(clusters.begin(), Analyzing::clusters);
  Json::Value json;

  json["no_months"] = Constants::months_range;
  json["no_vacancies"] = Reading::vacancies.size();

  json["skills"] = Json::arrayValue;
  int index = 0;
  for (Skill *skill : Reading::skills) {
    Json::Value temp;
    temp["index"] = index++;
    temp["k"] = skill->k;
    temp["n"] = skill->n;
    temp["name"] = skill->name;
    temp["no_occurrences"] = skill->no_occurrences;
    temp["no_different_clusters"] = skill->no_different_clusters;
    temp["avg_rank"] = skill->avg_rank;

    temp["graph"] = Json::arrayValue;
    for (int i = 0; i < Constants::months_range; ++i)
      temp["graph"].append(skill->graph[i]);

    temp["popularity"] = Json::arrayValue;
    for (int i = 0; i < Constants::months_range; ++i)
      temp["popularity"].append(skill->popularity[i]);

    json["skills"].append(temp);
  }

  index = 0;
  for (Cluster *cluster : clusters)
    cluster->index = index++;

  json["clusters"] = Json::arrayValue;
  for (Cluster *cluster : clusters) {
    Json::Value temp;

    temp["id"] = cluster->index;
    temp["k"] = cluster->k;
    temp["n"] = cluster->n;
    temp["greatness"] = cluster->greatness;
    temp["depth"] = cluster->depth;
    temp["no_vacancies"] = cluster->vacancies.size();

    temp["children"] = Json::arrayValue;
    for (Cluster *child : cluster->children)
      temp["children"].append(child->index);

    temp["skills"] = Json::arrayValue;
    for (SkillInCluster *skill : cluster->skills) {
      Json::Value temp2;
      temp2["index"] = skill->i;
      temp2["TF"] = skill->TF;
      temp2["TFIDF"] = skill->TFIDF;
      temp["skills"].append(temp2);
    }

    temp["graph"] = Json::arrayValue;
    for (int i = 0; i < Constants::months_range; ++i)
      temp["graph"].append(cluster->graph[i]);

    json["clusters"].append(temp);
  }

  std::ofstream out(constants.output_path);
  out << json << std::endl;
  out.close();

  print("Output saved to " + constants.output_path);
}


// SAVING K-MEANS

void save_clusters(std::ofstream &file, k_means::Cluster *cluster) {

  // vacancies
  file << cluster->vacancies.size() << " ";
  for (Vacancy *vacancy : cluster->vacancies)
    file << vacancy->index << " ";

  // children
  file << cluster->children.size() << " ";
  for (k_means::Cluster *child : cluster->children)
    save_clusters(file, child);
}

/**
 * len_vacancies
 *   vacancy_id
 *
 * len_children
 *   child
 */
void Export::save_k_means(k_means::Cluster *root) {
  std::ofstream file(constants.clusters_path);
  save_clusters(file, root);
  file.close();
}


// READING K-MEANS

Cluster *readCluster(std::ifstream &file, std::vector<Vacancy *> &vacancies, int depth = 0) {
  int no_vacancies;
  file >> no_vacancies;

  if (no_vacancies == -1)
    throw std::invalid_argument("Clusters file is corrupted.");

  auto *cluster = new Cluster(depth);

  for (int i = 0; i < no_vacancies; ++i) {
    int vacancy;
    file >> vacancy;

    if (vacancy >= vacancies.size())
      throw std::invalid_argument("Vacancy " + str(vacancy) + " is out of range.");
    if (vacancies[vacancy] == nullptr)
      throw std::invalid_argument("Vacancy " + str(vacancy) + " is null.");

    cluster->vacancies.push_back(vacancies[vacancy]);
  }

  int no_children;
  file >> no_children;
  for (int j = 0; j < no_children; ++j)
    cluster->children.push_back(readCluster(file, vacancies, depth + 1));

  return cluster;
}

void Export::read_k_means() {
  std::vector<Vacancy *> vacancies(Reading::vacancies.begin(), Reading::vacancies.end());
  std::ifstream clusters_file(constants.clusters_path);
  Analyzing::root = readCluster(clusters_file, vacancies);
}
