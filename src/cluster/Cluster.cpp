#include "Cluster.h"

void Cluster::selectSkills() {
  auto iter = skills.begin();
  for (int i = 0; i < Constants::max_skills_in_cluster && iter != skills.end(); ++i)
    ++iter;
  if (iter != skills.end())
    skills.erase(iter, skills.end());
}

void Cluster::normalizeTFIDF() {
  double sum = 0;
  for (SkillInCluster *skill : skills)
    sum += skill->TFIDF;
  if (sum > 0)
    for (SkillInCluster *skill : skills)
      skill->TFIDF /= sum;
}

void Cluster::sortSkills() {
  skills.sort([](const SkillInCluster *a, const SkillInCluster *b) {
    return a->TFIDF > b->TFIDF;
  });
}

void Cluster::calculateGreatness() {
  int limit = Constants::skills_in_greatness;
  double sum = 0;
  for (SkillInCluster *skill : skills) {
    if (!limit--) break;
    greatness += skill->skill->avg_rank / skill->TFIDF;
    sum += 1. / skill->TFIDF;
  }
  greatness /= sum;
}
