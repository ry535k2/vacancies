#ifndef CLUSTER_H
#define CLUSTER_H

#include "../k-means/K-means.h"

class SkillInCluster {
 public:
  int TF = 0, i;  // index in `skills`
  double TFIDF = 0;
  Skill *skill;

  explicit SkillInCluster(int i) : i(i) {
    skill = Reading::skills[i];
  };
};

class Cluster {
 public:
  std::list<SkillInCluster *> skills;
  std::list<Vacancy *> vacancies;
  std::list<Cluster *> children;
  int index = -1, depth;
  double greatness = 0, k = 0, n = 0;
  std::vector<double> graph;

  explicit Cluster(int depth) : depth(depth) {}

  void selectSkills();

  void normalizeTFIDF();

  void sortSkills();

  void calculateGreatness();
};

#endif  // CLUSTER_H
