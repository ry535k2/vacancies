#include "src/export/Export.h"

int main() {

  Time t1("reading vacancies");
  Reading::read();
  t1.out();

  // running K-means
  if (Constants::force_clustering || !std::ifstream(constants.clusters_path)) {
    Time t2("K-means");
    k_means::K_means k;
    t2.out();

    Time t3("saving");
    Export::save_k_means(k.root);
    t3.out();
  }

  Time t4("reading clusters");
  Export::read_k_means();
  t4.out();

  Time t5("analyzing");
  Analyzing analyzing;
  t5.out();

  Time t6("exporting");
  Export temp;
  t6.out();
}